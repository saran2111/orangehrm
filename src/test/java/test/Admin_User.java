package test;

import org.java.LaunchBrowser;
import org.java.Login;
import org.java.Logout;
import org.java.Pim_Reports;
import org.java.UserManage;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class Admin_User {
	@BeforeMethod
	public void open() {
		LaunchBrowser.openBrowser();
		LaunchBrowser.openWebsite();
		Login open = new Login();
		open.loginKeys();

	}

	@Test
	public void usermanageTest() {
		UserManage manage = new UserManage();
		manage.usermangKeys();
	}

	@AfterMethod
	public void close() {
		Logout close = new Logout();
		close.logout_Keys();
		LaunchBrowser.close();

	}

}
