package test;

import org.java.LaunchBrowser;
import org.java.Login;
import org.java.Logout;
import org.java.Tab_Options;
import org.java.Time_Timesheet;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class Time_Timesheet_test {

	@BeforeMethod
	public void open() {
		LaunchBrowser.openBrowser();
		LaunchBrowser.openWebsite();
		Login open = new Login();
		open.loginKeys();
		Tab_Options option=new Tab_Options();
		option.time();
	}

	@Test
	public void timesheet_Test() {
		Time_Timesheet time = new Time_Timesheet();
		time.timesheet_Keys();

	}

	@AfterMethod
	public void close() {
		Logout close = new Logout();
		close.logout_Keys();
		LaunchBrowser.close();

	}

}
