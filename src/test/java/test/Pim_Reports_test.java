package test;

import org.java.LaunchBrowser;
import org.java.Login;
import org.java.Logout;
import org.java.Pim_Reports;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class Pim_Reports_test {
	@BeforeMethod
	public void open() {
		LaunchBrowser.openBrowser();
		LaunchBrowser.openWebsite();
		Login open = new Login();
		open.loginKeys();
		
	}

	@Test
	private void reportTest() {
		Pim_Reports rep = new Pim_Reports();
		rep.reportKeys();

	}

	@AfterMethod
	public void close() {
		Logout close = new Logout();
		close.logout_Keys();
		LaunchBrowser.close();

	}

	
}

