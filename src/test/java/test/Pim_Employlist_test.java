package test;

import org.java.LaunchBrowser;
import org.java.Login;
import org.java.Logout;
import org.java.Pim_Employlist;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class Pim_Employlist_test {
	@BeforeMethod
	private void open() {
		LaunchBrowser.openBrowser();
		LaunchBrowser.openWebsite();
		Login open = new Login();
		open.loginKeys();

	}

	@Test
	private void pim_test() {
		Pim_Employlist emply = new Pim_Employlist();
		emply.employ_keys();

	}

	@AfterMethod
	public void close() {
		Logout close = new Logout();
		close.logout_Keys();
		LaunchBrowser.close();

	}

}
