package org.java;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Job extends Login {
	@FindBy(xpath="(//span[@class='oxd-topbar-body-nav-tab-item'])[2]") WebElement job;
	@FindBy(xpath="//a[text()='Job Titles']") WebElement jobTitle;
	@FindBy(xpath="//i[contains(@class,'oxd-icon bi-plus')]") WebElement add;
	@FindBy(xpath="(//input[@class='oxd-input oxd-input--active'])[2]") WebElement title;
	@FindBy(xpath="//textarea[@placeholder='Type description here']") WebElement jobDesc;
	@FindBy(xpath="//textarea[@placeholder='Add note']") WebElement note;
	@FindBy(xpath="//button[@type='submit']") WebElement submit;
	@FindBy(xpath="//div[text()='Software Tester']/../preceding-sibling::div") WebElement clickBox;
	@FindBy(xpath="//span[text()='Design & Execution of test scripts']/../../following-sibling::div//button") WebElement delete;
	@FindBy(xpath="//i[contains(@class,'oxd-icon bi-trash ')]") WebElement ok;
	public Job() {
		
	PageFactory.initElements(driver, this);
	}
	

public void jobKeys() {
	
}
}

