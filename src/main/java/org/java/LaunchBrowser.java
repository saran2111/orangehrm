package org.java;

import java.awt.AWTException;
import java.awt.Robot;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

import io.github.bonigarcia.wdm.WebDriverManager;

public class LaunchBrowser {
	public static WebDriver driver;
	public static Robot robot = null;

	public static WebDriver getDriver() {
		return driver;
	}

	public static void openBrowser() {
		WebDriverManager.chromedriver().setup();
		driver = new ChromeDriver();
		driver.manage().window().maximize();

		try {
			robot = new Robot();
		} catch (AWTException e) {

			e.printStackTrace();
		}

	}

	public static void openWebsite() {
		driver.get("https://opensource-demo.orangehrmlive.com/");

	}

	public static void close() {
		driver.quit();

	}

}
