package org.java;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class UserManage extends Login {
	@FindBy(xpath = "//span[text()='Admin']")
	WebElement admin;

	@FindBy(xpath = "(//button[contains(@class,'oxd-button oxd-button')])[3]")
	WebElement addUser;
	@FindBy(xpath = "(//div[@class='oxd-select-text-input'])[1]")
	WebElement userRole;
	@FindBy(xpath = "(//div[@class='oxd-select-wrapper'])[2]")
	WebElement status;

	@FindBy(xpath = "//input[@placeholder='Type for hints...']")
	WebElement emplyName;
	@FindBy(xpath = "(//label[text()='Username']//following::input)[1]")
	WebElement user;
	@FindBy(xpath = "(//input[@type='password'])[1]")
	WebElement pass1;
	@FindBy(xpath = "(//input[@type='password'])[2]")
	WebElement pass2;
	@FindBy(xpath = "//button[@type='submit']")
	WebElement save;
	@FindBy(xpath = ("//input[@class='oxd-input oxd-input--active'])[2]"))
	WebElement user_search;
	@FindBy(xpath = "(//div[@class='oxd-select-text--after'])[1]")
	WebElement drop;
	@FindBy(xpath = "//input[@placeholder='Type for hints...']")
	WebElement name_search;
	@FindBy(xpath = "(//div[@class='oxd-select-text-input'])[2]")
	WebElement status_search;
	@FindBy(xpath = "//button[@type='submit']")
	WebElement search;
	
	

	//
	public UserManage() {
		// openBrowser();
		// openWebsite();

		PageFactory.initElements(driver, this);
	}
	//

	public void usermangKeys() {
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		admin.click();
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		addUser.click();
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		status.click();
		Robot robot = null;
		try {
			robot = new Robot();
		} catch (AWTException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		robot.keyPress(KeyEvent.VK_DOWN);
		robot.keyRelease(KeyEvent.VK_DOWN);
		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);
		userRole.click();
		robot.keyPress(KeyEvent.VK_DOWN);
		robot.keyRelease(KeyEvent.VK_DOWN);
		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);
		emplyName.sendKeys("Alice Duval");
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		robot.keyPress(KeyEvent.VK_DOWN);
		robot.keyPress(KeyEvent.VK_DOWN);
		robot.keyPress(KeyEvent.VK_DOWN);
		robot.keyRelease(KeyEvent.VK_DOWN);
		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);

		pass1.click();
		pass1.sendKeys("Ram1234@");
		pass2.click();
		pass2.sendKeys("Ram1234@");
		save.click();
		user.click();
		user.sendKeys("arunkumar");

		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		save.click();

	}

}
