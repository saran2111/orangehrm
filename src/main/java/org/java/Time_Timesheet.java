package org.java;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Time_Timesheet extends Login{
	@FindBy(xpath = "//span[text()='Time']")
	WebElement time;
	@FindBy(xpath="(//div[text()='Employee Name']/../following::div/div//div//div/div)[1]") WebElement emplName_table;
	@FindBy(xpath = "//input[@placeholder='Type for hints...']")
	WebElement emplyName;
	@FindBy(xpath = "//button[@type='submit']")
	WebElement view;
	public Time_Timesheet() {
		
		PageFactory.initElements(driver,this);
		
	}
	public void timesheet_Keys() {
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		time.click();
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		String text = emplName_table.getText();
		System.out.println(text);
		emplyName.sendKeys(text);
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		robot.keyPress(KeyEvent.VK_DOWN);
		robot.keyRelease(KeyEvent.VK_DOWN);

		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);
		view.click();

	}
}

