package org.java;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import io.github.bonigarcia.wdm.WebDriverManager;

public class Logout {
	//public static WebDriver driver;
	//logout
		@FindBy(className="oxd-userdropdown-name")WebElement click_out;
		@FindBy(xpath="//a[text()='Logout']") WebElement logout;
		public Logout() {
//			WebDriverManager.chromedriver().setup();
			PageFactory.initElements(LaunchBrowser.driver, this);

		}

		public void logout_Keys() {
			try {
				Thread.sleep(4000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			click_out.click();
			logout.click();
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

}
