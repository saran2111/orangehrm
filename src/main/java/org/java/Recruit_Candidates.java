package org.java;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Recruit_Candidates extends Login {
	@FindBy(xpath = "//span[text()='Recruitment']")
	WebElement recruit;
	@FindBy(xpath = "//i[contains(@class,'oxd-icon bi-plus')]")
	WebElement add;
	@FindBy(xpath = "//input[@placeholder='First Name']")
	WebElement fname;
	@FindBy(xpath = "//input[@placeholder='Last Name']")
	WebElement lname;
	@FindBy(xpath = "(//div[@class='oxd-select-text--after'])[1]")
	WebElement down;
	@FindBy(xpath = "(//label[text()='Email']//following::input)[1]")
	WebElement email;
	@FindBy(xpath = "(//label[text()='Email']//following::input)[2]")
	WebElement phone_num;
	@FindBy(xpath = "//textarea[@placeholder='Type here']")
	WebElement notes;
	@FindBy(xpath = "//button[text()=' Save ']")
	WebElement save;
	@FindBy(xpath="//a[text()='Candidates']") WebElement  cand;
	@FindBy(xpath="(//div[@class='oxd-select-text--after'])[2]") WebElement vacancy ;
	@FindBy(xpath="(//div[@class='oxd-select-text--after'])[3]") WebElement manager;
	@FindBy(xpath="(//div[@class='oxd-select-text--after'])[4]") WebElement status_search ;
	@FindBy(xpath="//input[@placeholder='Type for hints...']") WebElement  search_name;
	@FindBy(xpath="//button[text()=' Search ']") WebElement search;
	@FindBy(xpath = "(//span[contains(@class,'oxd-checkbox-input oxd-checkbox-input')])[2]")
	WebElement box;
	@FindBy(xpath = "//i[@class='oxd-icon bi-trash']")
	WebElement delete;
	@FindBy(xpath = "//i[contains(@class,'oxd-icon bi-trash ')]")
	WebElement confDelete;

	public Recruit_Candidates() {
		PageFactory.initElements(driver, this);
	}

	public void recruit_Keys() {
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		recruit.click();
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		add.click();
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		fname.sendKeys("ArunKumar");

		lname.sendKeys("P");
		down.click();

		for (int i = 1; i <= 5; i++) {
			robot.keyPress(KeyEvent.VK_DOWN);
			robot.keyRelease(KeyEvent.VK_DOWN);

		}
		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);
		email.sendKeys("aryn@gmail.com");
		phone_num.sendKeys("9876543211");
		notes.sendKeys("hiytgh");
		save.click();
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		cand.click();
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		vacancy.click();
		for (int i = 1; i <= 5; i++) {
			robot.keyPress(KeyEvent.VK_DOWN);
			robot.keyRelease(KeyEvent.VK_DOWN);

		}
		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);
		manager.click();
		robot.keyPress(KeyEvent.VK_DOWN);
		robot.keyRelease(KeyEvent.VK_DOWN);
		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);
		status_search.click();
		robot.keyPress(KeyEvent.VK_DOWN);
		robot.keyRelease(KeyEvent.VK_DOWN);
		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);
		search_name.sendKeys("ArunKumar");
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {

			e.printStackTrace();
		}
		robot.keyPress(KeyEvent.VK_DOWN);
		robot.keyRelease(KeyEvent.VK_DOWN);
		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);
		search.click();
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {

			e.printStackTrace();
		}
		// checkbox
		box.click();
		// delete
		delete.click();
		confDelete.click();

	}

}
