package org.java;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.github.dockerjava.core.dockerfile.DockerfileStatement.Add;

public class Pim_Reports extends Login {
	@FindBy(xpath = "//span[text()='PIM']")
	WebElement pim;
	@FindBy(xpath = "//a[text()='Reports']")
	WebElement report;
	@FindBy(xpath = "//i[contains(@class,'oxd-icon bi-plus')]")
	WebElement add;
	@FindBy(xpath = "(//input[contains(@class,'oxd-input ox')])[2]")
	WebElement repName;
	@FindBy(xpath = "(//div[@class='oxd-select-text-input'])[3]")
	WebElement disGroup;
	@FindBy(xpath = "(//div[@class='oxd-select-text-input'])[4]")
	WebElement disField;
	@FindBy(xpath = "(//i[@class=\"oxd-icon bi-plus\"])[2]")
	WebElement plus;
	@FindBy(xpath = "//button[text()=' Save ']")
	WebElement save;

	@FindBy(xpath = "//input[@placeholder='Type for hints...']")
	WebElement reportName;
	@FindBy(xpath = "//button[@type='submit']")
	WebElement search;
	@FindBy(xpath = "(//span[contains(@class,'oxd-checkbox-input oxd-checkbox-input')])[2]")
	WebElement box;
	@FindBy(xpath = "//i[@class='oxd-icon bi-trash']")
	WebElement delete;
	@FindBy(xpath = "//i[contains(@class,'oxd-icon bi-trash ')]")
	WebElement confDelete;

	public Pim_Reports() {
		PageFactory.initElements(driver, this);
	}

	public void reportKeys() {
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		pim.click();

		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {

			e.printStackTrace();
		}

		report.click();

		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {

			e.printStackTrace();
		}
		add.click();
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {

			e.printStackTrace();
		}
		repName.sendKeys("PIM Original Report");
		// display group
		disGroup.click();
		Robot robot = null;
		try {
			robot = new Robot();
		} catch (AWTException e) {

			e.printStackTrace();
		}
		for (int i = 1; i <= 2; i++) {
			robot.keyPress(KeyEvent.VK_DOWN);
			robot.keyRelease(KeyEvent.VK_DOWN);

		}
		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);
		// display field
		disField.click();
		for (int i = 1; i <= 3; i++) {
			robot.keyPress(KeyEvent.VK_DOWN);
			robot.keyRelease(KeyEvent.VK_DOWN);

		}
		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);
		plus.click();
		save.click();
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {

			e.printStackTrace();
		}
		// report
		report.click();

		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		// report name
		reportName.sendKeys("PIM Original Report");
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {

			e.printStackTrace();
		}
		robot.keyPress(KeyEvent.VK_DOWN);
		robot.keyPress(KeyEvent.VK_DOWN);
		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);
		search.click();
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {

			e.printStackTrace();
		}
		// checkbox
		box.click();
		// delete
		delete.click();
		confDelete.click();

	}

}
