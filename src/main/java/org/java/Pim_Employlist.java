package org.java;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Pim_Employlist extends Login {
	@FindBy(xpath = "//span[text()='PIM']")
	WebElement pim;
	@FindBy(xpath = "//i[contains(@class,'oxd-icon bi-plus')]")
	WebElement add;
	@FindBy(name = "firstName")
	WebElement fname;
	@FindBy(name = "lastName")
	WebElement lname;
	@FindBy(xpath = "//button[@type='submit']")
	WebElement submit;
	@FindBy(xpath = "//a[text()='Employee List']")
	WebElement employList;
	@FindBy(xpath = "//input[@placeholder='Type for hints...']")
	WebElement employname;
	@FindBy(xpath = "//button[@type='submit']")
	WebElement search;
	@FindBy(xpath = "(//span[contains(@class,'oxd-checkbox-input oxd-')])[2]")
	WebElement box;
	@FindBy(xpath = "//i[@class='oxd-icon bi-trash']")
	WebElement delete;
	@FindBy(xpath = "//i[contains(@class,'oxd-icon bi-trash ')]")
	WebElement confDelete;

	public Pim_Employlist() {

		PageFactory.initElements(driver, this);
	}

	public void employ_keys() {
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		pim.click();
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		add.click();
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		fname.click();
		fname.sendKeys("Arunkumar");
		lname.click();
		lname.sendKeys("P");
		submit.click();
		try {
			Thread.sleep(6000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		employList.click();
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		employname.click();
		employname.sendKeys("Arunkumar P");
		search.click();
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		box.click();
		delete.click();
		confDelete.click();
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
