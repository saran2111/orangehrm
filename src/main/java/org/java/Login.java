package org.java;

import java.awt.AWTException;
import java.awt.Robot;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import io.github.bonigarcia.wdm.WebDriverManager;

public class Login extends LaunchBrowser {

	@FindBy(xpath = "//input[@name='username']")
	WebElement username;
	@FindBy(xpath = "//input[@name='password']")

	WebElement password;
	@FindBy(xpath = "//button[@type='submit']")

	WebElement submit;

	public Login() {
		// openBrowser();
		// openWebsite();
		PageFactory.initElements(driver, this);

	}

	public void loginKeys() {
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		username.sendKeys("Admin");
		password.click();
		password.sendKeys("admin123");
		submit.click();

	}

}
