package org.java;

import java.awt.Robot;
import java.awt.AWTException;
import java.awt.Desktop.Action;
import java.awt.event.KeyEvent;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import com.beust.ah.A;

public class Leave extends Login {
	@FindBy(xpath = "(//input[@placeholder='yyyy-mm-dd'])[1]")
	WebElement sdate;
	@FindBy(xpath = "(//input[@placeholder='yyyy-mm-dd'])[2]")
	WebElement todate;
	
	@FindBy(xpath = "//div[text()='Select']")
	WebElement select;
	@FindBy(xpath = "(//div[contains(@class,'oxd-select-text oxd-select')])[2]")
	WebElement canperson;
	@FindBy(xpath = "(//div[contains(@class,'oxd-select-text oxd-select')])[3]")
	WebElement select2;
	@FindBy(xpath = "//input[@placeholder='Type for hints...']")
	WebElement employName;
	@FindBy(xpath = "//button[@type='submit']")
	WebElement search;

	public Leave() {
		loginKeys();
		PageFactory.initElements(driver, this);
	}

	private void leaveKeys() {
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		
		sdate.clear();

		sdate.sendKeys("2023-05-24");
//		todate.click();
//		todate.clear();
//		todate.sendKeys("2023-05-26");
		select.click();
		Robot robot = null;
		try {
			robot = new Robot();
		} catch (AWTException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		for (int i = 1; i <= 5; i++) {
			robot.keyPress(KeyEvent.VK_DOWN);
			robot.keyRelease(KeyEvent.VK_DOWN);

		}

		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//
		canperson.click();
		for (int j = 1; j <= 3; j++) {
			robot.keyPress(KeyEvent.VK_DOWN);
			robot.keyRelease(KeyEvent.VK_DOWN);

		}

		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);
		//
		select2.click();
		for (int k = 1; k <= 4; k++) {
			robot.keyPress(KeyEvent.VK_DOWN);
			robot.keyRelease(KeyEvent.VK_DOWN);

		}

		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);
		//
		employName.sendKeys("Charlie Carter");
		search.click();

	}

	public static void main(String[] args) {
		Leave l = new Leave();
		l.leaveKeys();

	}

}
